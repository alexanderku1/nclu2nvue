# nclu2nvue

NCLU to NVUE migration script.


## NVUE Release Changes

This is how NVUE releases are mapped to the public CL releases

| CL Version | Nvue version | 
| -----------|--------------|
| 4.4        | 0.21.06.18.1 |
| 5.0        | 0.21.12.07.0 |
| 5.1        | 0.22.05.05.0 |

To find out the diffs go here: 
https://wikinox.mellanox.com/pages/viewpage.action?spaceKey=SW&title=CUE+-+Releases

For each release collect the "list-commands and OM schema tree diff" and save them in the `./versions/<version>` directory. For example, release 0.22.02.09.0 (rolling into CL 5.1) introduced a number of CLI changes documented in commit @551a3c66. This is what you do:

1. `git clone ssh://git@gitlab-master.nvidia.com:12051/nbu-sws/CL/CUE/cue.git && cd cue`
2. `COMMIT=551a3c6610e090a31468aed267e8412ed4a0e545 && VERSION=0.22.02.09.0`
3. `git --no-pager diff $COMMIT~1:nv-list-commands.txt $COMMIT:nv-list-commands.txt > $VERSION.diff`
4. `mv $VERSION.diff ~/nclu2nvue/versions/5.1/`

The complete list of available commands (not diffs) can be found this way:

1. `git clone ssh://git@gitlab-master.nvidia.com:12051/nbu-sws/CL/CUE/cue.git && cd cue`
2. `git checkout tags/release-5.1.0`
3. `cp nv-list-commands.txt ~/nclu2nvue/versions/5.1`

## NCLU Commands

All NCLU commands are stored in [./tests/nclu/list-commands.txt](./tests/nclu/list-commands.txt).

## Making Changes

Describe the add/change/delete process and the corresponding changes to tests


## Deployment/Release Process

Currently the script runs in a container deployed in AIR at https://air.nvidia.com/migrate/. In order to release a new version you need to:

1. Create a new branch and push the required changes into it.
2. Make sure Gitlab-CI completes and all tests are green.
3. Merge the PR into the `master` branch and record the commit hash.
4. Go to https://gitlab-master.nvidia.com/nbu-sws/air/airschipp and update hash of repo in CI/CD variables
![image](./image.png)

5. Manually run CI pipeline in https://gitlab-master.nvidia.com/nbu-sws/air/airschipp which automatically creates and deploys new container into Air

For all logs from production and any questions about the AIR CI pipline contact Nick Mitchell.