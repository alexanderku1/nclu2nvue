#!/bin/env python3.7
# Start date 24/05/2021
# Convert4-5 python script
# Version 0.3 / Dated 27/05/2021 / bad coding by Michael Schipp
# Version 0.4 / Dated 06/06/2021 / added net add dns and net add syslog
# Version 0.5 / Dated 06/06/2021 / fixes dns,ntp etc
# Version 0.6 / Dated 04/08/2021 / refactor code, finished up bgp and interfaces
# Version 0.6.1 / Dated 05/08/2021 / Fixed debug mode, filtered static nat
# Version 0.7 / Dated 24/08/2021 / Fixed bond bugs, pulled in merges for VXLAN and VNI
# Version 0.7.1 / Dated 25/08/2021 / Fixed post-up and not supported commands
# Version 0.7.2 / Dated 31/08/2021 / Fixed post-up and not supported commands
# Version 0.7.3 / Dated 01/09/2021 / L2VPN EVPN IPv4 advertise unicast
# Version 0.8 / Dated 22/09/2021 / DHCP Relay, Breakouts, MLAG backup IP, new comments/tags
# Version 0.9 / Dated 22/10/2021 / refactoring Hostname and BGP for CL5.0 restructure
# Version 1.0 / Dated 20/1/2022 / Fixing BGP static networking. Renamed variable fron CUE to NVUE
# Version 1.1 / Dated 21/1/2022 / Fixing merge typos, and hostname logic
# Version 1.2 / Dated 21/1/2022 / Fixing hostname logic

# Unsupported configs:
# net add time ntp source
# alias and descriptions
# time zone
# ptp features beyond priority and domain-number
# all - replace with _
# nv set interface peerlink.4094 clag args --redirectEnable
# net add nat static

### COMMENTS
# Any line that is not converted will be addressed with one of the following tags:
#  # FUTURE SUPPORT - Feature coming to NVUE in the future
#  # UNREQUIRED - Command no longer necessary in NVUE
#  # MANUAL REVIEW - Requires manual review of command before entry, it may not have been captured by the script
#  # SCRIPT UNSUPPORTED - Script doesn't handle input, and most likely not a relevant command in NVUE



import sys, getopt, subprocess
#set global variable
current_ver = '1.2'
NCLU = 'net'
NVUE = 'nv'
NVUE_SET   = NVUE + ' set '
NVUE_UNSET = NVUE + ' unset '
DEBUG = False
inputfile = '\n'
outputfile = 'output.txt'

# L2 VNI struct -- maps VNI interfaces to VXLAN ID and VLAN ID
VNI_STRUCT = {
   'dummy': {
      'vni': '5010',
      'vlan': '10'
   }
}

UNIQUE_PREFIX_STRINGS = set()

# L3 VNI set -- a set of VLAN IDs for L3 VNIs for special treatment
# It relies on the fact that 'net add vrf RED vni' comes before 'net add vlan'
L3_VNIs = set()


# Helper function that runs a shell command and returns its output as string
def run_shell_cmd(cmd):
   r = subprocess.run(cmd, capture_output=True, shell=True, text=True)
   return r.stdout

# Checks systemd for a particular process p and returns the vrf name as string
def check_vrf(p):
   cmd = f'systemctl --no-wall --no-legend --no-pager list-units {p}*'
   result = run_shell_cmd(cmd)
   parts = result.split('@')
   if len(parts) == 2:
       #['ntp.server', 'mgmt.service']
      return parts[1].split('.')[0]
   if len(parts) == 1:
      return 'default'
   else:
       raise Exception(f'unexpected systemd unit format')

def handle_prefix_string(pstr, str_list):
   if not pstr in UNIQUE_PREFIX_STRINGS:
       UNIQUE_PREFIX_STRINGS.add(pstr)
       str_list.append(pstr)

def main(argv):
   if not argv:     #check if no arg are entered
      print ('convert.py -i <inputfile> -o <outputfile> -c')
      print ('\n')
      print ('Usage:')
      print ('Input file must be specificed by using -i')
      print ('If -o is not specified then default of output.txt is used')
      print ('Optional -c will use cl set instead of the default of nv set')
      print ('Optional -d will debug the output')
      sys.exit()    # no arg, bugging out of here
   global inputfile,outputfile,NVUE,NVUE_SET,NVUE_UNSET,DEBUG  #use global variable
   try:
      opts, args = getopt.getopt(argv,'hi:o:c:d',['ifile=','ofile='])
   except getopt.GetoptError:
      print ('convert.py -i <inputfile> -o <outputfile> -c')
      print ('\n')
      print ('Usage:')
      print ('If -o is not specified then dafault of output.txt is used')
      print ('-c will use cl set, default is to use nv set')
      print ('-d will output debugged output line by line')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print ('convert.py -i <inputfile> -o <outputfile> -c')
         print ('\n')
         print ('Usage:')
         print ('If -o is not specified then dafault of output.txt is used')
         print ('-c will use cl set, default is to use nv set')

         sys.exit()
      elif opt in ('-i', '--ifile'):
         inputfile = arg
      elif opt in ('-o', '--ofile'):
         outputfile = arg
      elif opt == '-c':
         NVUE = 'cl'

      elif opt == '-d':
         DEBUG = True
if __name__ == '__main__':
   main(sys.argv[1:])

line_output = []
debug_output = []
line_str = ''
temp_list = []
write_line = True
input_file = open(inputfile,'r')            # open the file for reading only
in_data = input_file.readlines()            # read the file into a list
output_file = open(outputfile,'w')           # open the file for writing to

if DEBUG:
   debug_file = open('debug.txt', 'w')

title = '#!/bin/bash\n#Converted from 4.x to 5.1\nset -x\n'    # add comment to file to say what created this file
output_file.write(title)

for line_data in in_data:
   line_str = line_data

   orig_str = line_str

   lines_str = line_str.strip()
   if not lines_str:
      continue

   if line_str.startswith('#'):
      continue

   # make sure everything is single spaced because script only works with single spacing
   line_str = line_str.replace('  ',' ')

   if line_str.startswith('net commit'):
      line_str= '# UNREQUIRED - ' + line_str
   elif not line_str.startswith('net'):
      line_str= '# SCRIPT UNSUPPORTED - ' + line_str

   ### Pre-filter all VRF to replace `-` with `_`
   if line_str.find(' vrf ') != -1:
      line_str = line_str.replace(line_str.split('vrf')[1].split()[0],line_str.split('vrf')[1].split()[0].replace('-','_'))

   ### Not implemented commands
   if line_str.find('pim')!=-1:
      line_str = line_str.replace('net add ', '# Not supported (PIM) - ')

   #### IPv6 filtered because future plans
   #### IPv6 interface features
   if line_str.startswith('net add interface') or line_str.startswith('net add bond') or line_str.startswith('net add interface') or line_str.startswith('net add bond'):
      if line_str.find('ipv6 nd prefix') > 0:
         temp_list = line_str.split()
         base_str = NVUE_SET + temp_list[2] + ' ' + temp_list[3] + ' ip neighbor-discovery prefix ' + temp_list[7]
         aux_args = []
         num_args = []
         future_support = False
         for n in range(8, len(temp_list)):
            if temp_list[n].isnumeric():
               num_args.append(temp_list[n]) # valid-lifetime / preferred-lifetime
            elif temp_list[n] == 'off-link':
               aux_args.append('off-link on')
            elif temp_list[n] == 'no-autoconfig':
               aux_args.append('autoconfig off')
            else:  ## TODO?
               future_support = True
               break

         if not future_support:
            line_str_list = []
            handle_prefix_string(base_str + '\n', line_str_list)

            if len(num_args) >= 2:
               handle_prefix_string(base_str + ' valid-lifetime '     + num_args[0] + '\n', line_str_list)
               handle_prefix_string(base_str + ' preferred-lifetime ' + num_args[1] + '\n', line_str_list)

            for aux_arg in aux_args:
               handle_prefix_string(base_str + ' ' + aux_arg + '\n', line_str_list)

            line_str = ''.join(line_str_list)
         else:
            line_str = '# FUTURE SUPPORT - ' + line_str

   if line_str.find('ipv6 nd suppress-ra') > 0:
      temp_list = line_str.split()
      if temp_list[1] == 'add':
         line_str = NVUE_SET + 'interface ' + temp_list[3] + ' ip neighbor-discovery router-advertisement enable off' + '\n'
      else:
         line_str = NVUE_SET + 'interface ' + temp_list[3] + ' ip neighbor-discovery router-advertisement enable on'  + '\n'

   if line_str.find('ipv6 nd ra-interval') > 0:
      temp_list = line_str.split()
      print(temp_list)
      if temp_list[7] == "msec":
         line_str = NVUE_SET + 'interface ' + temp_list[3] + ' ip neighbor-discovery router-advertisement interval ' + temp_list[8] + '\n'
      else:
         line_str = NVUE_SET + 'interface ' + temp_list[3] + ' ip neighbor-discovery router-advertisement interval ' + str(int(temp_list[7]) * 1000) + '\n'

   if line_str.find('ipv6 nd adv-interval-option') > 0:
      temp_list = line_str.split()
      if temp_list[1] == 'add':
         line_str = NVUE_SET + 'interface ' + temp_list[3] + ' ip neighbor-discovery router-advertisement interval-option on'  + '\n'
      else:
         line_str = NVUE_SET + 'interface ' + temp_list[3] + ' ip neighbor-discovery router-advertisement interval-option off' + '\n'

   if line_str.find('ipv6 nd ra-fast-retrans') > 0:
      temp_list = line_str.split()
      if temp_list[1] == 'add':
         line_str = NVUE_SET + 'interface ' + temp_list[3] + ' router-advertisement fast-retransmit on'  + '\n'
      else:
         line_str = NVUE_SET + 'interface ' + temp_list[3] + ' router-advertisement fast-retransmit off' + '\n'

   if line_str.find('ipv6 nd ra-lifetime') > 0:
      temp_list = line_str.split()
      line_str = NVUE_SET + 'interface ' + temp_list[3] + ' ip neighbor-discovery router-advertisement lifetime ' + temp_list[7] + '\n'

   if line_str.find('ipv6 nd reachable-time') > 0:
      temp_list = line_str.split()
      line_str = NVUE_SET + 'interface ' + temp_list[3] + ' ip neighbor-discovery router-advertisement reachable-time ' + temp_list[7] + '\n'

   if line_str.find('ipv6 nd router-preference') > 0:
      temp_list = line_str.split()
      line_str = NVUE_SET + 'interface ' + temp_list[3] + ' ip neighbor-discovery router-advertisement router-preference ' + temp_list[7] + '\n'

   if line_str.find('ipv6 nd home-agent-lifetime') > 0:
      temp_list = line_str.split()
      line_str = NVUE_SET + 'interface ' + temp_list[3] + ' ip neighbor-discovery home-agent lifetime '   + temp_list[7] + '\n'

   if line_str.find('ipv6 nd home-agent-preference') > 0:
      temp_list = line_str.split()
      line_str = NVUE_SET + 'interface ' + temp_list[3] + ' ip neighbor-discovery home-agent preference ' + temp_list[7] + '\n'

   if line_str.find('ipv6 nd mtu') > 0:
      temp_list = line_str.split()
      line_str = NVUE_SET + 'interface ' + temp_list[3] + ' ip neighbor-discovery mtu ' + temp_list[7] + '\n'

   ### Static NAT filtered because not supported
   elif line_str.startswith('net add nat static'):
      line_str = '# FUTURE SUPPORT - ' + line_str

   ###SERVICES
   #### Hostname
   if line_str.startswith('net add hostname'):
      line_str = line_str.replace('net add hostname', NVUE_SET + 'system hostname')
   #### SNMP
   # SNMP is not yet implemented so replace configs with blank line
   elif line_str.startswith('net add snmp-server'):
      line_str = '# FUTURE SUPPORT - ' + line_str
   #### NTP
   elif line_str.startswith('net add time ntp server'):
      # net add time ntp server 0.cumulusnetworks.pool.ntp.org iburst
      # nv set service ntp mgmt server 0.cumulusnetworks.pool.ntp.org iburst on
      vrf = check_vrf('ntp')
      line_str = line_str.replace('net add time ntp server', f'{NVUE_SET} service ntp {vrf} server')
      line_str = line_str.replace('iburst', f'iburst on')
   elif line_str.startswith('net add time ntp source'):
      line_str = '# FUTURE SUPPORT - ' + line_str
   elif line_str.startswith('net add time zone'):
      line_str = '# FUTURE SUPPORT - ' + line_str
   #### PTP
   elif line_str.startswith('net add ptp global priority1'):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'service ptp 1 priority1 ' + temp_list[-1] + '\n'
   elif line_str.startswith('net add ptp global priority2'):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'service ptp 1 priority1 ' + temp_list[-1] + '\n'
   elif line_str.startswith('net add ptp global domain-number'):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'service ptp 1 domain ' + temp_list[-1] + '\n'
   elif line_str.startswith('net add ptp global slave-only no'):
      line_str = '# FUTURE SUPPORT - ' + line_str
   elif line_str.startswith('net add ptp global logging-level 5'):
      line_str = '# FUTURE SUPPORT - ' + line_str
   elif line_str.startswith('net add ptp global path-trace-enabled no'):
      line_str = '# FUTURE SUPPORT - ' + line_str
   elif line_str.startswith('net add ptp global use-syslog yes'):
      line_str = '# FUTURE SUPPORT - ' + line_str
   elif line_str.startswith('net add ptp global verbose no'):
      line_str = '# FUTURE SUPPORT - ' + line_str
   elif line_str.startswith('net add ptp global summary-interval 0'):
      line_str = '# FUTURE SUPPORT - ' + line_str
   elif line_str.startswith('net add ptp global time-stamping'):
      line_str = '# FUTURE SUPPORT - ' + line_str
   #### DNS yes VRF
   elif line_str.startswith('net add dns nameserver') and (line_str.find('vrf')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'service dns '+ temp_list[-1] +' server ' +temp_list[5] + '\n'
   #### DNS no VRF
   elif line_str.startswith('net add dns nameserver') and (line_str.find('vrf')==-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'service dns default server ' +temp_list[-1] + '\n'
   #### Syslog yes VRF
   elif line_str.startswith('net add syslog') and line_str.startswith('vrf'):
      line_str = line_str.replace('net add syslog host',NVUE_SET + 'service syslog')
      line_str = line_str.replace('port ','')
   #### Syslog no VRF
   elif line_str.startswith('net add syslog'):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'service syslog default server ' + temp_list[-4] + ' port ' + temp_list[-1] + '\n' + \
         NVUE_SET + 'service syslog default server ' + temp_list[-4] + ' protocol ' + temp_list[-2] + '\n'
   #### DHCP Relay in default VRF
   elif line_str.startswith('net add dhcp relay server'):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'dhcp-relay default server ' + temp_list[-1] + '\n'
   elif line_str.startswith('net add dhcp relay interface'):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'dhcp-relay default interface ' + temp_list[-1] + '\n'
   #### Dot1x
   elif line_str.startswith('net add dot1x'):
      line_str = '# FUTURE SUPPORT - ' + line_str

   ### Platform and User Accounts
   #### User management
   elif line_str.startswith('net add username'):
      line_str = '# FUTURE SUPPORT - ' + line_str

   ###Routing Defaults
   elif line_str.startswith('net add routing defaults datacenter'):
      line_str = '# UNREQUIRED - ' + line_str
   elif line_str.startswith('net add routing log syslog'):
      line_str = '# UNREQUIRED - ' + line_str
   elif line_str.startswith('net add routing service integrated-vtysh-config'):
      line_str = '# UNREQUIRED - ' + line_str

   ###Static Routes
   #### no VRF
   elif line_str.startswith('net add routing route ') and (line_str.find('vrf')==-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf default router static ' + temp_list[4] + ' via ' + temp_list[5] + '\n'
   #### yes VRF
   elif line_str.startswith('net add routing route ') and (line_str.find('vrf') != -1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf ' + temp_list[-1] +' router static ' + temp_list[4] + ' via ' + temp_list[5] + '\n'

   ### Route-maps
   elif line_str.startswith('net add routing route-map'):
      temp_list = line_str.split()
      if line_str.find('prefer-global') > 0: #next-hop prefer-global
         #nv set router policy route-map example rule 1 set ipv6-nexthop-prefer-global on
         line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' set ipv6-nexthop-prefer-global ' + 'on' + '\n'
      else:
         line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' action ' + temp_list[5] + '\n' + \
         NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule '+ temp_list[6] + ' ' + temp_list[-3] + ' ' + temp_list[-2] + ' ' + temp_list[-1] + '\n'

   elif line_str.startswith('net del routing route-map'):
      temp_list = line_str.split()
      if line_str.find('prefer-global') > 0: #next-hop prefer-global
         line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' set ipv6-nexthop-prefer-global ' + 'off' + '\n'

   ### PBR maps
   #### nexthop-group
   elif line_str.startswith('net add pbr-map'):
      temp_list = line_str.split()
      if line_str.find('nexthop-group') > 0:
         line_str = NVUE_SET + 'router pbr map ' + temp_list[3] + ' rule ' + temp_list[5] + ' set nexthop-group ' + temp_list[8] + '\n'

   ### BGP
   #### Autonomous System no VRF
   elif line_str.startswith('net add bgp autonomous-system'):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf default router bgp autonomous-system ' + temp_list[-1] + '\n'
   #### Autonomous System yes VRF
   elif line_str.startswith('net add bgp vrf') and (line_str.find('autonomous-system') != -1):
      temp_list = line_str.split()
      line_str = NVUE +' set vrf ' + temp_list[4] +' router bgp autonomous-system ' + temp_list[-1] + '\n'
   #### VRF and VNI mapping
   elif line_str.startswith('net add vrf') and (line_str.find('vni') != -1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf ' + temp_list[3] + ' evpn vni ' + temp_list[-1] + '\n'
      L3_VNIs.add(temp_list[-1])
   #### Router-ID
   ##### no VRF
   elif line_str.startswith('net add bgp router-id'):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf default router bgp router-id ' + temp_list[-1] +'\n'
   ##### yes VRF
   elif line_str.startswith('net add bgp vrf') and (line_str.find('router-id') != -1):
      temp_list = line_str.split()
      line_str = NVUE +' set vrf ' + temp_list[4] +' router bgp router-id ' + temp_list[-1] + '\n'
   #### Bestpath as-path multipath relax
   ##### no VRF
   elif line_str.startswith('net add bgp bestpath as-path multipath-relax'):
      line_str = NVUE_SET +'vrf default router bgp path-selection multipath aspath-ignore on\n'
   ##### yes VRF
   elif line_str.startswith('net add bgp vrf') and (line_str.find('bestpath as-path multipath-relax')!=-1):
      temp_list = line_str.split()
      line_str = NVUE +' set vrf ' + temp_list[4] + ' router bgp path-selection multipath aspath-ignore on\n'
   #### Neighbor
   ##### Peer-group no VRF
   elif line_str.startswith('net add bgp neighbor') and (line_str.find('peer-group')!=-1) and (line_str.find('interface')==-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf default router bgp peer-group ' + temp_list[-2]+'\n'
   ##### Peer-group yes VRF
   elif line_str.startswith('net add bgp vrf') and (line_str.find('peer-group')!=-1) and (line_str.find('interface')==-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf ' + temp_list[3] + ' router bgp peer-group ' + temp_list[-2]+'\n'
   ##### Peer-group remote-as no VRF
   elif line_str.startswith('net add bgp neighbor') and (line_str.find('remote-as')!=-1) and (line_str.find('interface')==-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf default router bgp peer-group ' + temp_list[-3] + ' remote-as ' + temp_list[-1] + '\n'
   ##### Peer-group remote-as yes VRF
   elif line_str.startswith('net add bgp vrf') and (line_str.find('remote-as')!=-1) and (line_str.find('interface')==-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf ' + temp_list[3] + ' router bgp peer-group ' + temp_list[-3] + ' remote-as ' + temp_list[-1] + '\n'
   ##### description
   elif line_str.startswith('net add bgp neighbor') and (line_str.find('description')!=-1):
      line_str = '# FUTURE SUPPORT - ' + line_str
   ##### BFD
   elif line_str.startswith('net add bgp neighbor') and (line_str.find('bfd')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf default router bgp neighbor ' + temp_list[4] + ' bfd enable on\n' + \
      NVUE_SET + 'vrf default router bgp neighbor ' + temp_list[4] + ' bfd detect-multiplier ' + temp_list[-3] + '\n' + \
      NVUE_SET + 'vrf default router bgp neighbor ' + temp_list[4] + ' bfd min-rx-interval ' + temp_list[-2] + '\n' + \
      NVUE_SET + 'vrf default router bgp neighbor ' + temp_list[4] + ' bfd min-tx-interval ' + temp_list[-1] +'\n'
   ##### Extended-nexthop
   elif line_str.startswith('net add bgp neighbor') and (line_str.find('capability extended-nexthop')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf default router bgp peer-group ' + temp_list[4] + ' capabilities extended-nexthop on' + '\n'
   ##### Peer-group membership no VRF
   elif line_str.startswith('net add bgp neighbor') and (line_str.find('interface peer-group')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf default router bgp neighbor ' + temp_list[4] + ' type unnumbered\n' + \
      NVUE_SET + 'vrf default router bgp neighbor ' + temp_list[4] + ' peer-group ' + temp_list[-1]+'\n'
   ##### Peer-group membership yes VRF
   elif line_str.startswith('net add bgp vrf') and (line_str.find('interface peer-group')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf ' + temp_list[4] + ' router bgp neighbor ' + temp_list[4] + ' type unnumbered\n' + \
      NVUE_SET + 'vrf ' + temp_list[4] + ' router bgp neighbor ' + temp_list[4] + ' peer-group ' + temp_list[-1]+'\n'
   ##### remote-as interface no VRF
   elif line_str.startswith('net add bgp neighbor') and (line_str.find('interface remote-as')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf default router bgp neighbor ' + temp_list[4] + ' type unnumbered\n' + \
      NVUE_SET + 'vrf default router bgp neighbor ' + temp_list[4] + ' remote-as ' + temp_list[-1]+'\n'
   ##### remote-as interface yes VRF
   elif line_str.startswith('net add bgp vrf') and (line_str.find('interface remote-as')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf default ' + temp_list[4] + ' bgp neighbor ' + temp_list[4] + ' type unnumbered\n' + \
      NVUE_SET + 'vrf ' + temp_list[4] + ' router bgp neighbor ' + temp_list[4] + ' remote-as ' + temp_list[-1]+'\n'
   #### IPv4 unicast address family
   ##### network no VRF
   elif line_str.startswith('net add bgp ipv4 unicast network'):
      line_str = line_str.replace('net add bgp ipv4 unicast network',NVUE + ' set vrf default router bgp address-family ipv4-unicast network')
   ##### network yes VRF
   elif line_str.startswith('net add bgp vrf') and (line_str.find('ipv4 unicast network')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf ' + temp_list[4] + ' router bgp address-family ipv4-unicast network ' + temp_list[-1] + '\n'
   ##### redistribute no VRF
   elif line_str.startswith('net add bgp ipv4 unicast redistribute'):
      temp_list = line_str.split()
      if line_str.find('route-map') > 0:
         line_str = NVUE_SET + 'vrf default router bgp address-family ipv4-unicast redistribute ' + temp_list[-3] + ' route-map ' + temp_list[-1] + '\n'
      else:
         line_str = NVUE_SET + 'vrf default router bgp address-family ipv4-unicast redistribute ' + temp_list[-1] +'\n'
   ##### redistribute yes VRF
   elif line_str.startswith('net add bgp vrf') and (line_str.find('ipv4 unicast redistribute')!=-1):
      temp_list = line_str.split()
      if line_str.find('route-map') > 0:
         line_str = NVUE_SET + 'vrf ' + temp_list[4] + ' router bgp address-family ipv4-unicast redistribute ' + temp_list[-3] + ' route-map ' + temp_list[-1] + '\n'
      else:
         line_str = NVUE_SET + 'vrf ' + temp_list[4] + ' router bgp address-family ipv4-unicast redistribute ' + temp_list[-1] +'\n'
   ##### next-hop-self
   elif line_str.startswith('net add bgp ipv4 unicast neighbor') and (line_str.find('next-hop-self')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'vrf default router bgp neighbor ' + temp_list[-2] + ' address-family ipv4-unicast nexthop-setting self\n'
   #### allowas-in
   elif line_str.startswith('net add bgp ipv4 unicast neighbor') and (line_str.find('allowas-in origin')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'vrf default router bgp peer-group ' + temp_list[-3] + ' address-family ipv4-unicast aspath allow-my-asn origin on\n'
   #### IPv6 unicast address family
   elif line_str.startswith('net add bgp ipv6 unicast neighbor') and (line_str.find('activate')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'vrf default router bgp peer-group ' + temp_list[-2] + ' address-family ipv6-unicast enable on\n'
   #### L2VPN EVPN address family
   ##### Activate
   elif line_str.startswith('net add bgp l2vpn evpn neighbor peerlink.4094 activate'):
      line_str = NVUE_SET + 'vrf default router bgp neighbor peerlink.4094 address-family l2vpn-evpn enable on\n'
   elif line_str.startswith('net add bgp l2vpn evpn neighbor') and (line_str.find('activate')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'vrf default router bgp peer-group ' + temp_list[-2] + ' address-family l2vpn-evpn enable on\n' + \
      NVUE_SET + 'vrf default router bgp address-family l2vpn-evpn enable on\n'
   elif line_str.startswith('net add bgp l2vpn evpn neighbor swp') and (line_str.find('activate')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'vrf default router bgp neighbor' + temp_list[7] + 'address-family l2vpn-evpn enable on\n'
   ##### advertise-all-vni
   elif line_str.startswith('net add bgp l2vpn evpn advertise-all-vni'):
      line_str = NVUE_SET + 'evpn enable on\n'
   ##### advertise-svi-ip generic
   elif line_str.startswith('net add bgp l2vpn evpn advertise-svi-ip'):
      temp_list = line_str.split()
      line_str = NVUE_SET +'evpn route-advertise svi-ip on\n'
   ##### advertise-svi-ip VNI
   elif line_str.startswith('net add bgp l2vpn evpn vni') and (line_str.find('advertise-svi-ip')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET +'evpn evi ' + temp_list[6] +' route-advertise svi-ip on\n'
   ##### advertise ipv4 unicast in vrf
   elif line_str.startswith('net add bgp vrf') and (line_str.find('l2vpn evpn advertise ipv4 unicast') != -1):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'vrf ' + temp_list[4] + ' router bgp address-family ipv4-unicast route-export to-evpn\n'
   elif line_str.startswith('net add bgp vrf') and (line_str.find('ipv4 unicast import vrf route-map') > 0):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'vrf ' + temp_list[4] + ' router bgp address-family ipv4-unicast route-import from-vrf route-map ' + temp_list[10] + '\n'
   elif line_str.startswith('net add bgp vrf') and (line_str.find('ipv4 unicast import vrf') > 0):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'vrf ' + temp_list[4] + ' router bgp address-family ipv4-unicast route-import from-vrf list ' + temp_list[9] + '\n'
   elif line_str.startswith('net add bgp vrf') and (line_str.find('neighbor') > 0) and (line_str.find('graceful-restart-mode helper-and-restarter') > 0):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'vrf ' + temp_list[4] + ' router bgp peer-group ' + temp_list[6] + ' graceful-restart mode full' + '\n'
   elif line_str.startswith('net add bgp vrf') and (line_str.find('neighbor') > 0) and (line_str.find('graceful-restart-mode helper') > 0):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'vrf ' + temp_list[4] + ' router bgp peer-group ' + temp_list[6] + ' graceful-restart mode helper-only' + '\n'
   elif line_str.startswith('net add bgp vrf') and (line_str.find('neighbor') > 0) and (line_str.find('description') > 0):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'vrf ' + temp_list[4] + ' router bgp peer-group ' + temp_list[6] + ' description ' + ' '.join(temp_list[8:]) + '\n' #can description be mutiple strings?
   ### Interface
   ### Interface definitions
   elif line_str.startswith('net add interface swp') != -1 and len(line_str.split()) == 4:
      line_str = line_str.replace('net add interface', NVUE_SET + 'interface')
   #### breakout
   elif line_str.startswith('net add interface') and (line_str.find('breakout')!=-1):
      temp_list = line_str.split()
      ### Commenting this section about because breakout configs will require manual review
      # if temp_list[-1] == '4x' or temp_list[-2] == '2x':
      #    line_str = '# NOT SUPPORTED SYNTAX ' + line_str
      # else:
      #    line_str = NVUE_SET + 'interface ' + temp_list[3] + ' link breakout ' + temp_list[-1] + '\n'
      line_str = '# MANUAL REVIEW ' + NVUE_SET + 'interface ' + temp_list[3] + ' link breakout ' + temp_list[-1] + '\n'
   ##### MTU
   elif line_str.startswith('net add interface') and (line_str.find('mtu')!=-1):
      line_str = line_str.replace('net add ',NVUE_SET)
      line_str = line_str.replace('mtu','link mtu')
   ##### VRF membership:
   elif line_str.startswith('net add interface') and (line_str.find('vrf')!=-1) and (line_str.find('peerlink')==-1):
      line_str = line_str.replace('net add ',NVUE_SET)
      line_str = line_str.replace('vrf','ip vrf')
   ### EVPN MH link settings
   ##### EVPN MH Uplink:
   # net add interface swp51-54 evpn mh uplink -> nv set interface swp51-54 evpn multihoming uplink on
   elif line_str.startswith('net add interface') and (line_str.find('evpn mh uplink')!=-1):
      line_str = line_str.replace('net add ',NVUE_SET)
      line_str = line_str.replace('mh uplink','multihoming uplink on')
   ##### EVPN DF preference for bonds only:
   # net add bond bond1-3 evpn mh es-df-pref 50000 -> nv set interface bond1-3 evpn multihoming segment df-preference 50000
   elif line_str.startswith('net add bond') and (line_str.find('evpn mh es-df-pref')!=-1):
      line_str = line_str.replace('net add ',NVUE_SET)
      line_str = line_str.replace(' bond ',' interface ')
      line_str = line_str.replace('mh es-df-pref','multihoming segment df-preference')
   ##### EVPN ESI MAC for bonds only:
   # net add bond bond1-3 evpn mh es-sys-mac 44:38:39:be:ef:aa -> nv set interface bond1 evpn multihoming segment mac-address <mac>
   elif line_str.startswith('net add') and (line_str.find('evpn mh es-sys-mac')!=-1):
      line_str = line_str.replace('net add ',NVUE_SET)
      line_str = line_str.replace(' bond ',' interface ')
      line_str = line_str.replace('mh es-sys-mac','multihoming segment mac-address')
   ##### EVPN ESI ID for bonds only:
   # net add bond bond1 evpn mh es-id 1 -> nv set interface bond1 evpn multihoming segment local-id 1
   elif line_str.startswith('net add') and (line_str.find('evpn mh es-id')!=-1):
      line_str = line_str.replace('net add ',NVUE_SET)
      line_str = line_str.replace(' bond ',' interface ')
      line_str = line_str.replace('mh es-id','multihoming segment local-id')
   ##### EVPN MH startup delay:
   # net add evpn mh startup-delay 10 -> nv set evpn  multihoming startup-delay
   elif line_str.startswith('net add evpn mh startup-delay'):
      line_str = line_str.replace('net add ',NVUE_SET)
      line_str = line_str.replace('mh startup-delay','multihoming startup-delay')
   #### Bridge
   ##### bridge pvid
   elif line_str.startswith('net add interface') and (line_str.find('bridge pvid')!=-1):
      line_str = line_str.replace('net add interface',NVUE_SET + 'interface')
      line_str = line_str.replace('bridge pvid','bridge domain br_default untagged')
   ##### bridge vids
   elif line_str.startswith('net add interface') and (line_str.find('bridge vids')!=-1):
      line_str = line_str.replace('net add interface',NVUE_SET + 'interface')
      line_str = line_str.replace('bridge vids','bridge domain br_default vlan')
   ##### bridge access
   elif line_str.startswith('net add interface') and (line_str.find('bridge access')!=-1):
      line_str = line_str.replace('net add interface',NVUE_SET + 'interface')
      line_str = line_str.replace('bridge access','bridge domain br_default access')
   ##### stp features
   elif line_str.startswith('net add interface') and (line_str.find('stp')!=-1):
      line_str = line_str.replace('net add interface',NVUE_SET + 'interface')
      line_str = line_str.replace('stp bpduguard','bridge domain br_default stp bpdu-guard on')
      line_str = line_str.replace('stp portadminedge','bridge domain br_default stp admin-edge on')
      line_str = line_str.replace('stp portautoedge','bridge domain br_default stp auto-edge on')
      line_str = line_str.replace('stp portbpdufilter','bridge domain br_default stp bpdu-filter on')
      line_str = line_str.replace('stp portnetwork','bridge domain br_default stp network on')
      line_str = line_str.replace('stp portrestrole','bridge domain br_default stp restrole on')
   #### Bond
   elif line_str.startswith('net add bond'):
      temp_list = line_str.split()
      if not temp_list[3].startswith('bond'):
         line_str = line_str.replace(temp_list[3], temp_list[3].replace('-','_')) + \
           NVUE_SET + 'interface ' + temp_list[3].replace('-','_') + ' type bond\n'
      ##### members
      if line_str.startswith('net add bond') and (line_str.find('bond slaves')!=-1):
         line_str = line_str.replace('net add bond', NVUE_SET +'interface')
         line_str = line_str.replace('bond slaves', 'bond member')
      ##### alias
      elif line_str.startswith('net add bond') and (line_str.find('alias')!=-1):
         line_str = '# FUTURE SUPPORT - ' + line_str
      ##### bridge pvid
      elif line_str.startswith('net add bond') and (line_str.find('bridge pvid')!=-1):
         line_str = line_str.replace('net add bond',NVUE_SET + 'interface')
         line_str = line_str.replace('bridge pvid','bridge domain br_default untagged')
      ##### bridge vids
      elif line_str.startswith('net add bond') and (line_str.find('bridge vids')!=-1):
         line_str = line_str.replace('net add bond',NVUE_SET + 'interface')
         line_str = line_str.replace('bridge vids','bridge domain br_default vlan')
      ##### bridge access
      elif line_str.startswith('net add bond') and (line_str.find('bridge access')!=-1):
         line_str = line_str.replace('net add bond',NVUE_SET + 'interface')
         line_str = line_str.replace('bridge access','bridge domain br_default access')
      ##### bridge allow-untagged
      elif line_str.startswith('net add bond') and (line_str.find('bridge allow-untagged no')!=-1):
         line_str = line_str.replace('net add bond',NVUE_SET + 'interface')
         line_str = line_str.replace('bridge allow-untagged','bridge domain br_default untagged none')
      ##### bridge allow-untagged
      elif line_str.startswith('net add bond') and (line_str.find('bond lacp-bypass-allow')!=-1):
         line_str = line_str.replace('net add bond', NVUE_SET + 'interface')
         line_str = line_str.replace('lacp-bypass-allow','lacp-bypass on')
      ##### CLAG/MLAG
      elif line_str.startswith('net add bond') and (line_str.find('clag id')!=-1):
         line_str = line_str.replace('net add bond',NVUE_SET + 'interface')
         line_str = line_str.replace('clag id','bond mlag id')
      ##### MTU
      elif line_str.startswith('net add bond') and (line_str.find('mtu')!=-1):
         line_str = line_str.replace('net add bond',NVUE_SET + 'interface')
         line_str = line_str.replace('mtu','link mtu')
      ##### STP
      elif line_str.startswith('net add bond') and (line_str.find('stp')!=-1):
         line_str = line_str.replace('net add bond',NVUE_SET + 'interface')
         line_str = line_str.replace('stp','bridge domain br_default stp')
         line_str = line_str.replace('bpduguard','bpdu-guard on')
         line_str = line_str.replace('portadminedge','admin-edge on')
         line_str = line_str.replace('portautoedge','auto-edge on')
         line_str = line_str.replace('portbpdufilter','bpdu-filter on')
         line_str = line_str.replace('portnetwork','network on')
         line_str = line_str.replace('portrestrrole','restrrole on')
   #### eth0 vrf mgmt
   elif line_str.startswith('net add interface eth'):
      if line_str.find('alias')!=-1:
         line_str = '# FUTURE SUPPORT - ' + line_str
      else:
         line_str = line_str.replace('net add ', NVUE_SET)
         line_str = line_str.replace('vrf mgmt', 'ip vrf mgmt')
      if line_str.find('post-up') > 0:
         line_str = '# FUTURE SUPPORT - ' + line_str

      if line_str.find('ip forward') > 0:
         line_str = line_str.replace('ip forward', 'ip ipv4 forward')
      elif line_str.find('ip6-forward') > 0:
         line_str = line_str.replace('ip6-forward', 'ip ipv6 forward')
   #### VXLAN VNI
   elif line_str.startswith('net add vxlan') and (line_str.find('arp-nd-suppress')!=-1):
      line_str = 'nv set nve vxlan arp-nd-suppress on\n'
   elif line_str.startswith('net add vxlan') and (line_str.find('vxlan id')!=-1):
      # net add vxlan vni10 vxlan id 10
      temp_list = line_str.split()
      vni_intf = VNI_STRUCT.get(temp_list[3], dict())
      vni_intf['vni'] = temp_list[6]
      VNI_STRUCT[temp_list[3]] = vni_intf
      line_str = f'# Command handled via VNI_STRUCT (see bottom of the file) -- {line_str}'
   elif line_str.startswith('net add vxlan') and (line_str.find('bridge access')!=-1):
      # net add vxlan vni10 bridge access 10
      temp_list = line_str.split()
      vni_intf = VNI_STRUCT.get(temp_list[3], dict())
      vni_intf['vlan'] = temp_list[6]
      VNI_STRUCT[temp_list[3]] = vni_intf
      line_str = f'# Command handled via VNI_STRUCT (see bottom of the file) -- {line_str}'
   elif line_str.startswith('net add vxlan'):
      line_str = '\n'
   #### alias # net add interface swp51-54 alias to spine
   elif line_str.startswith('net add interface') and (line_str.find('alias')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'interface ' + temp_list[3] + ' link state up \n'
   elif line_str.startswith('net add interface') and (line_str.find('bridge vids')!=-1):
      line_str = line_str.replace('bridge vids','bridge domain br_default vlan')
   #### vxlan-anycast-ip
   elif line_str.startswith('net add loopback lo clag vxlan-anycast-ip'):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'nve vxlan mlag shared-address ' + temp_list[-1] + '\n'
   #### local tunnelip # net add loopback lo vxlan local-tunnelip 10.10.10.2
   elif line_str.startswith('net add loopback lo vxlan local-tunnelip'):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'nve vxlan source address ' + temp_list[-1] + '\n'
   #### Loopback
   elif line_str.startswith('net add loopback lo'):
      if (line_str.find('alias')!=-1):
         line_str = '# FUTURE SUPPORT - ' + line_str
      else:
         line_str = line_str.replace('net add loopback',NVUE_SET + 'interface')
   #### hwaddress
   elif line_str.startswith('net add vlan') and (line_str.find('hwaddress')!=-1):
      temp_list = line_str.split()
      #line_str = NVUE_SET + 'interface vlan' + temp_list[3] + ' ip vrr mac-address ' + temp_list[-1] + '\n' + \
      #NVUE_SET + 'interface vlan' + temp_list[3] + ' ip vrr state up '+'\n'
      line_str = f'# UNREQUIRED - {line_str}'

   #### VLAN
   ##### alias
   elif line_str.startswith('net add vlan') and (line_str.find('alias')!=-1):
      line_str = '# FUTURE SUPPORT - ' + line_str
   ##### ip address (vrr) # net add vlan 30 ip address 10.1.30.3/24
   elif line_str.startswith('net add vlan') and (line_str.find('ip address')!=-1) and (line_str.find('address-virtual')==-1):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'interface' + ' vlan' + temp_list[3] + ' ' + ' '.join(temp_list[4:]) + '\n'
   ##### ip forward
   elif line_str.startswith('net add vlan') and (line_str.find('ip forward')!=-1):
      temp_list = line_str.split()
      line_str = line_str.replace('net add vlan','net add interface')
      line_str = line_str.replace(temp_list[3], 'vlan'+temp_list[3])
      line_str = line_str.replace('ip forward', 'ip ipv4 forward')
   ##### ip6 forward
   elif line_str.startswith('net add vlan') and (line_str.find('ip6-forward')!=-1):
      temp_list = line_str.split()
      line_str = line_str.replace('net add vlan','net add interface')
      line_str = line_str.replace(temp_list[3], 'vlan'+temp_list[3])
      line_str = line_str.replace('ip6-forward', 'ip ipv6 forward')
   ##### vrr address-virtual  # net add vlan 30 ip address-virtual 00:00:00:00:00:30 10.1.30.1/24
   elif line_str.startswith('net add vlan') and (line_str.find('ip address-virtual')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'interface vlan'+temp_list[3]+' ip vrr address ' + temp_list[-1]+'\n' + \
      NVUE_SET + 'interface vlan'+temp_list[3]+' ip vrr mac-address ' + temp_list[-2]+'\n' + \
      NVUE_SET + 'interface vlan'+temp_list[3]+' ip vrr state up '+'\n'
   ##### VLAN-id
   elif line_str.startswith('net add vlan') and (line_str.find('vlan-id')!=-1):
      temp_list = line_str.split()
      if not temp_list[3] in L3_VNIs:
         line_str = NVUE_SET + 'interface vlan'+temp_list[3]+' vlan '+temp_list[3]+'\n'
   #### vlan-raw-device
   elif line_str.startswith('net add vlan') and (line_str.find('vlan-raw-device')!=-1):
      line_str = '# UNREQUIRED - ' + line_str
   #### VRF #net add vlan 4001 vrf RED
   elif line_str.startswith('net add vlan') and (line_str.find('vrf')!=-1):
      temp_list = line_str.split()
      if not temp_list[3] in L3_VNIs:
         line_str = NVUE_SET + 'interface vlan'+temp_list[3]+' ip vrf '+temp_list[-1]+'\n'
   #### MTU
   elif line_str.startswith('net add vlan') and (line_str.find('mtu')!=-1):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'interface vlan'+temp_list[3]+' link mtu '+temp_list[-1]+'\n'

   ### VRF
   #### VRF definition
   elif line_str.startswith('net add vrf') and (line_str.find('vrf-table')!=-1):
      temp_list = line_str.split()
      vrf_list = temp_list[3].split(',')
      for vrf in vrf_list:
         line_str = NVUE_SET + 'vrf '+ vrf +'\n'
         line_output.extend(line_str)
         write_line = False
   #### VRF loopback
   elif line_str.startswith('net add vrf') and (line_str.find('ip')!=-1) and (line_str.find('address')!=-1):
      temp_list = line_str.split()
      vrf_list = temp_list[3].split(',')
      for vrf in vrf_list:
         line_str = NVUE_SET + 'vrf '+ vrf + ' loopback ip address ' + temp_list[-1] + '\n'
         line_output.extend(line_str)
         write_line = False

   ### Bridge
   #### Bridge ports
   elif line_str.startswith('net add bridge bridge ports') or line_str.startswith('net del bridge bridge ports'):
      add = line_str.find('add') != -1
      line_str = line_str.replace('net add bridge bridge ports ', '')
      line_str = line_str.replace('net del bridge bridge ports ', '')
      line_str = line_str.replace('\n', '\n')
      temp_list = line_str.split(',')
      for word in temp_list:
         if (word.find('vni')==-1):
            if add:
               line_str = NVUE_SET + 'interface ' + word.replace('\n','') + ' bridge domain br_default\n'
            else:
               line_str = NVUE_UNSET + 'interface ' + word.replace('\n','') + ' bridge\n'
            line_output.extend(line_str)
            write_line = False
   #### Bridge vids
   elif line_str.startswith('net add bridge bridge vids'):
      temp_str = line_str.split()
      line_str = NVUE_SET + 'bridge domain br_default vlan ' + temp_str[-1] + '\n'
   #### Bridge pvid
   elif line_str.startswith('net add bridge bridge pvid'):
      temp_str = line_str.split()
      line_str = NVUE_SET + 'bridge domain br_default untagged ' + temp_str[-1] + '\n'
   #### Bridge vlan-aware
   elif line_str.startswith('net add bridge bridge vlan-aware'):
      line_str = NVUE_SET + 'bridge domain br_default type vlan-aware' + '\n'
   #### Bridge STP priority
   elif line_str.startswith('net add bridge stp treeprio'):
      line_str = line_str.replace('net add bridge stp treeprio', 'nv set bridge domain br_default stp priority')


   ### CLAG/MLAG
   #### net add clag peer macro
   elif line_str.startswith('net add clag peer sys-mac'):
      temp_str = line_str.split()
      priority = 1000 if temp_str[8] == 'primary' else 2000
      line_output.extend(NVUE_SET + 'interface peerlink bond member ' + temp_str[7] + '\n')
      line_output.extend(NVUE_SET + 'mlag mac-address ' + temp_str[5]  + '\n')
      line_output.extend(NVUE_SET + 'mlag backup ' + temp_str[10]  + '\n')
      line_output.extend(NVUE_SET + 'mlag peer-ip linklocal' + '\n')
      line_output.extend(NVUE_SET + 'mlag priority ' + str(priority) + '\n')
      write_line = False

   #### backup ip VRF yes
   elif line_str.startswith('net add interface peerlink') and (line_str.find('clag backup-ip')!=-1) and (line_str.find('vrf mgmt')!=-1):
      temp_str = line_str.split()
      line_str = NVUE_SET + 'mlag backup ' + temp_str[-3] + ' vrf mgmt' + '\n'
   #### backup ip VRF no
   elif line_str.startswith('net add interface peerlink') and (line_str.find('clag backup-ip')!=-1) and (line_str.find('vrf mgmt')==-1):
      temp_str = line_str.split()
      line_str = NVUE_SET + 'mlag backup ' + temp_str[-1] + '\n'
   #### peerlink ip address
   elif line_str.startswith('net add interface peerlink') and (line_str.find('ip address')!=-1):
      line_str = '# UNREQUIRED - ' + line_str
   #### peer-ip
   elif line_str.startswith('net add interface peerlink') and (line_str.find('clag peer-ip')!=-1):
      temp_str = line_str.split()
      # line_str = NVUE_SET + 'mlag peer-ip ' + temp_str[-1] + '\n'
      # Moving code to mandatory use MLAG link local
      line_str = NVUE_SET + 'mlag peer-ip linklocal\n'
   #### priority
   elif line_str.startswith('net add interface peerlink') and (line_str.find('clag priority')!=-1):
      temp_str = line_str.split()
      line_str = NVUE_SET + 'mlag priority ' + temp_str[-1] + '\n'
   #### sys-mac
   elif line_str.startswith('net add interface peerlink') and (line_str.find('clag sys-mac')!=-1):
      temp_str = line_str.split()
      line_str = NVUE_SET + 'mlag mac-address ' + temp_str[-1] + '\n'
   #### args initDelay
   elif line_str.startswith('net add interface peerlink') and (line_str.find('initDelay')!=-1):
      temp_str = line_str.split()
      line_str = NVUE_SET + 'mlag init-delay ' + temp_str[-1] + '\n'
   #### args initDelay
   elif line_str.startswith('net add interface peerlink') and (line_str.find('redirectEnable')!=-1):
      line_str = '# FUTURE SUPPORT - ' + line_str


   ### OSPF
   elif line_str.startswith('net add ospf'):
      line_str = line_str.replace('net add ospf', NVUE +' set router ospf')

   ### bridge vids
   elif line_str.startswith('net add interface') and (line_str.find('bridge vids')!=-1):
      line_str = line_str.replace('net add',NVUE)
      line_str = line_str.replace('bridge vids','bridge domain br_default vlan')

   ### link speed
   elif line_str.startswith('net add interface') and (line_str.find('link speed')!=-1):
      line_str = line_str.replace('000','G')

   ### net add interface swp1 pbr-policy example
   elif line_str.startswith('net add interface') and (line_str.find('pbr-policy')!=-1):
      temp_str = line_str.split()
      line_str = NVUE_SET + 'interface ' + temp_str[3] + ' router pbr map ' + temp_str[5] + '\n'

   ### net add interface <> tunnel-
   elif line_str.startswith('net add interface') and (line_str.find('tunnel-endpoint') > 0):
      temp_str = line_str.split()
      #print(temp_str)
      line_str = NVUE_SET + 'interface ' + temp_str[3] + ' tunnel dest-ip ' + temp_str[5] + '\n'
   elif line_str.startswith('net add interface') and (line_str.find('tunnel-local') > 0):
      temp_str = line_str.split()
      line_str = NVUE_SET + 'interface ' + temp_str[3] + ' tunnel source-ip ' + temp_str[5] + '\n'
   elif line_str.startswith('net add interface') and (line_str.find('tunnel-mode') > 0):
      temp_str = line_str.split()
      line_str = NVUE_SET + 'interface ' + temp_str[3] + ' tunnel mode ' + temp_str[5] + '\n'
   elif line_str.startswith('net add interface') and (line_str.find('tunnel-ttl') > 0):
      temp_str = line_str.split()
      line_str = NVUE_SET + 'interface ' + temp_str[3] + ' tunnel ttl ' + temp_str[5] + '\n'

   ### Move the rest of unhandled net del into unsupported list
   if line_str.startswith('net del'):
      line_str = '# SCRIPT UNSUPPORTED - ' + line_str

   ### This is the catch all, any remaining `net add` was unhandled
   if line_str.startswith('net add '):
      line_str = line_str.replace('net add ', '# MANUAL REVIEW - net add ')

   if write_line:
      line_output.extend(line_str)
      #print (line_str)
      if DEBUG:
         print('DEBUG orig_str1: ' + orig_str)
         print('DEBUG line_str2: ' + line_str)
         debug_output.extend(orig_str + '-->\n\t' + line_str + '\n')
   else:
      write_line = True


# Create new VNI config
for k,v in VNI_STRUCT.items():
   # only create VNI mapping for L2 VNI's, excluding L3 VNIs (configured automatically)
   if 'vni' in k and v['vni'] and v['vlan'] and (not v['vlan'] in L3_VNIs):
      line_str = NVUE_SET + 'bridge domain br_default vlan ' + v['vlan'] + ' vni ' + v['vni'] + '\n'
      line_output.extend(line_str)

line_output.extend('\n\n')
line_output.extend('# EXECUTE MANUALLY: APPLY THE CONFIG - ' + NVUE + ' config apply' + '\n')
line_output.extend('# EXECUTE MANUALLY: SAVE  THE CONFIG - ' + NVUE + ' config save' + '\n')

for i in line_output:                       # go thouw line by line of the input file
    output_file.write(i)                    # write new output file
    #print(i)                               # dump it on the screen too
input_file.close()                          # close the input file
output_file.close()                         # close the output file
if DEBUG:
   for i in debug_output:
      debug_file.write(i)
   debug_file.close()

print ('Convert NCLU to NVUE script - version '+current_ver)
print('Using output type',NVUE)
print ('Output file is :', outputfile)
