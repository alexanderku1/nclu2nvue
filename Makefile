default: test

test-mlag:
	python3 ./convert.py -i tests/mlag-symm/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/mlag-symm/output
	@echo 'MLAG test passed'

test-mh-pim:
	python3 ./convert.py -i tests/evpn-mh-pim/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/evpn-mh-pim/output
	@echo 'EVPN MH PIM test passed'

test-leftovers:
	python3 ./convert.py -i tests/leftovers/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/leftovers/output
	@echo 'leftovers test passed'

test-cl51:
	python3 ./convert.py -i tests/cl51/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/cl51/output
	@echo 'CL51 test passed'

test: test-mlag test-mh-pim test-leftovers test-cl51

update-test-results:
	python3 ./convert.py -i tests/cl51/input -o tests/cl51/output
	python3 ./convert.py -i tests/leftovers/input -o tests/leftovers/output
	python3 ./convert.py -i tests/evpn-mh-pim/input -o tests/evpn-mh-pim/output
	python3 ./convert.py -i tests/mlag-symm/input -o tests/mlag-symm/output